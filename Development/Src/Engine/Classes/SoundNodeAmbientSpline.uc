/**
 * Copyright 1998-2011 Epic Games, Inc. All Rights Reserved.
 */
 
 /**
  * This class is similar to SoundNodeAmbient, but it uses attenuation model from SoundNodeAttenuationAndGain. 
  * SoundNodeAmbientSpline should be used by AmbientSoundSimpleSpline only.
  */

class SoundNodeAmbientSpline extends SoundNodeAttenuationAndGain
	native( Sound )
	hidecategories( Object )
	dependson( SoundNodeAmbient )
	editinlinenew;

var( Modulation )		float					PitchMin<ToolTip=The lower bound of pitch (1.0 is no change)>;
var( Modulation )		float					PitchMax<ToolTip=The upper bound of pitch (1.0 is no change)>;

var( Modulation )		float					VolumeMin<ToolTip=The lower bound of volume (1.0 is no change)>;
var( Modulation )		float					VolumeMax<ToolTip=The upper bound of volume (1.0 is no change)>;

var( Sounds )			array<AmbientSoundSlot>	SoundSlots<ToolTip=Sounds to play>;

defaultproperties
{
	VolumeMin=0.7
	VolumeMax=0.7
	PitchMin=1.0
	PitchMax=1.0
}

