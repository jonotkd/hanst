/**
 * Copyright 1998-2011 Epic Games, Inc. All Rights Reserved.
 */
 
/**
 * The class works similar to AmbientSoundSimple, but is uses spline to eval location of sound. And attenuation model from SoundNodeAttenuationAndGain, instead of SoundNodeAttenuation.
 */

class AmbientSoundSimpleSpline extends AmbientSoundSplineBase
	hidecategories( Audio )
	AutoExpandCategories( AmbientSoundSplineBase )
	native( Sound );

/** Mirrored property for easier editability, set in Spawned.		*/
var()	editinline editconst	SoundNodeAmbientSpline	AmbientProperties;
/** Dummy sound cue property to force instantiation of subobject.	*/
var		editinline export const SoundCue			SoundCueInstance;
/** Dummy sound node property to force instantiation of subobject.	*/
var		editinline export const SoundNodeAmbientSpline	SoundNodeInstance;

defaultproperties
{
	Begin Object NAME=Sprite
		Sprite=Texture2D'EditorResources.AmbientSoundIcons.S_Ambient_Sound_Simple'
		Scale=0.25
	End Object

	Begin Object Name=DrawSoundRadius0
		SphereColor=(R=0,G=102,B=255)
	End Object

	Begin Object Class=SoundNodeAmbientSpline Name=SoundNodeAmbient0
	End Object
	SoundNodeInstance=SoundNodeAmbient0

	Begin Object Class=SoundCue Name=SoundCue0
		SoundClass=Ambient
	End Object
	SoundCueInstance=SoundCue0
}
