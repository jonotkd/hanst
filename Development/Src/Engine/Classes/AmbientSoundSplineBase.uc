/**
 * Copyright 1998-2011 Epic Games, Inc. All Rights Reserved.
 */

/**
 * The source of sound is always placed in the nearest (to the listener's position) point on the spline.
 * https://udn.epicgames.com/Three/UsingSoundActors
 */ 

class AmbientSoundSplineBase extends AmbientSound
	
	AutoExpandCategories( AmbientSoundSplineBase )
	native( Sound );

/** SplineComponent with spline curve defining the source of sound */
var(AmbientSoundSplineBase)   SplineComponent     SplineComponent;

/** Only to test algorithm finding nearest point. */
var(AmbientSoundSplineBase) editoronly vector     TestPoint<ToolTip=For testing purpose only. By moving this point on level, man can see where the nearest point on spline is found.>;

/** Force all spline data to be consistent. */
native function UpdateSpline();

/** Recalculate spline after any control point was moved. */
native function UpdateSplineGeometry();

cpptext
{
	virtual void PostLoad();
	virtual void PostEditMove(UBOOL bFinished);
	virtual void PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent);
#if WITH_EDITOR
	virtual void EditorApplyTranslation(const FVector& DeltaTranslation, UBOOL bAltDown, UBOOL bShiftDown, UBOOL bCtrlDown);
#endif
}

defaultproperties
{
	Components.Remove( AudioComponent0 )

	Begin Object Class=SplineComponentSimplified Name=SplineComponent0
	End Object
	SplineComponent=SplineComponent0
	Components.Add( SplineComponent0 )

	Begin Object Class=SplineAudioComponent Name=AudioComponent1
		PreviewSoundRadius=DrawSoundRadius0
		bAutoPlay=false
		bStopWhenOwnerDestroyed=true
		bShouldRemainActiveIfDropped=true
		SplineComponent=SplineComponent0
	End Object
	AudioComponent=AudioComponent1
	Components.Add(AudioComponent1)
}