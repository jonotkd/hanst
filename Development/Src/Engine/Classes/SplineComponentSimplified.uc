/**
 * Copyright 1998-2011 Epic Games, Inc. All Rights Reserved.
 */

 /** 
  *  Regular SplineCompontne with few 'unused' categories hidden.
  */
class SplineComponentSimplified extends SplineComponent
	hidecategories( Rendering )
	hidecategories( Collision )
	hidecategories( Physics )
	hidecategories( Lighting )
	hidecategories( Object );