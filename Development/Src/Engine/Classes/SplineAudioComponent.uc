/**
 * Copyright 1998-2011 Epic Games, Inc. All Rights Reserved.
 */
class SplineAudioComponent extends AudioComponent
	native
	noexport
	collapsecategories
	hidecategories(Object,ActorComponent)
	dependson(ReverbVolume, SplineComponent)
	editinlinenew;

var SplineComponent SplineComponent;
