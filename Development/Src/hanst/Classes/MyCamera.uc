/**
 *	MyCamera
 *
 *	Creation date: 13/08/2011 14:14
 *	Copyright 2011, jonynz
 */
class MyCamera extends Camera;

var Vector CamOffset;
var float CameraZOffset;
var float CameraScale, CurrentCameraScale; /** multiplier to default camera distance */
var float CameraScaleMin, CameraScaleMax;

function UpdateViewTarget(out TViewTarget OutVT, float DeltaTime)
{
   local vector      HitLocation, HitNormal;
   local CameraActor   CamActor;
   local Pawn          TPawn;
   local Vector         Vector;
   local vector CamStart, CamDirX, CamDirY, CamDirZ, CurrentCamOffset;
   local float DesiredCameraZOffset;

   // Don't update outgoing viewtarget during an interpolation 
   if( PendingViewTarget.Target != None && OutVT == ViewTarget && BlendParams.bLockOutgoing )
   {
      return;
   }

   // Default FOV on viewtarget
   OutVT.POV.FOV = DefaultFOV;

   // Viewing through a camera actor.
   CamActor = CameraActor(OutVT.Target);
   if( CamActor != None )
   {
      CamActor.GetCameraView(DeltaTime, OutVT.POV);

      // Grab aspect ratio from the CameraActor.
      bConstrainAspectRatio   = bConstrainAspectRatio || CamActor.bConstrainAspectRatio;
      OutVT.AspectRatio      = CamActor.AspectRatio;

      // See if the CameraActor wants to override the PostProcess settings used.
      CamOverridePostProcessAlpha = CamActor.CamOverridePostProcessAlpha;
      CamPostProcessSettings = CamActor.CamOverridePostProcess;
   }
   else
   {
      TPawn = Pawn(OutVT.Target);
      // Give Pawn Viewtarget a chance to dictate the camera position.
      // If Pawn doesn't override the camera view, then we proceed with our own defaults
      if( TPawn == None || !TPawn.CalcCamera(DeltaTime, OutVT.POV.Location, OutVT.POV.Rotation, OutVT.POV.FOV) )
      {   
         /**************************************
          * Calculate third-person perspective
          * Borrowed from UTPawn implementation
          **************************************/
		  
		  //Top down
         OutVT.POV.Rotation = PCOwner.Rotation;    		    
		 // OutVT.POV.Rotation.Roll = 0;
		 // OutVT.POV.Rotation.Pitch = -16384;
		 // OutVT.POV.Location = TPawn.Location;
		 // OutVT.POV.Location.Z += 350.0f;
		 
		 //Normal
         camstart = tpawn.location;
         currentcamoffset = camoffset;
         
         desiredcamerazoffset = 0.0 * tpawn.getcollisionheight() + tpawn.mesh.translation.z;
         camerazoffset = (deltatime < 0.2) ? desiredcamerazoffset * 5 * deltatime + (1 - 5*deltatime) * camerazoffset : desiredcamerazoffset;
         
         camstart.z += camerazoffset;
         getaxes(outvt.pov.rotation, camdirx, camdiry, camdirz);
         camdirx *= currentcamerascale;
      
         tpawn.findspot(tpawn.getcollisionextent(),camstart);
         if (currentcamerascale < camerascale)
         {
            currentcamerascale = fmin(camerascale, currentcamerascale + 5 * fmax(camerascale - currentcamerascale, 0.3)*deltatime);
         }
         else if (currentcamerascale > camerascale)
         {
            currentcamerascale = fmax(camerascale, currentcamerascale - 5 * fmax(camerascale - currentcamerascale, 0.3)*deltatime);
         }                              
         if (camdirx.z > tpawn.getcollisionheight())
         {
            camdirx *= square(cos(outvt.pov.rotation.pitch * 0.0000958738)); // 0.0000958738 = 2*pi/65536
         }
         outvt.pov.location = camstart - camdirx*currentcamoffset.x + currentcamoffset.y*camdiry + currentcamoffset.z*camdirz;
         if (trace(hitlocation, hitnormal, outvt.pov.location, camstart, false, vect(12,12,12)) != none)
         {
            outvt.pov.location = hitlocation;
         }
		 
      }
   }

   // Apply camera modifiers at the end (view shakes for example)
   ApplyCameraModifiers(DeltaTime, OutVT.POV);
}

defaultproperties
{
   CamOffset=(X=12.0,Y=0.0,Z=-13.0)
   CurrentCameraScale=1.0
   CameraScale=9.0
   CameraScaleMin=3.0
   CameraScaleMax=40.0
}
