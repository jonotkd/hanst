class MyGameInfo extends FrameworkGame;

function Tick(float DeltaTime) // every frame, write a little messsage to screen so we know it is working
{
	//WorldInfo.Game.Broadcast(self, "Game updated in " $ DeltaTime);
	super.Tick(DeltaTime);
}

event PostLogin(PlayerController PC)
{
	local Pawn P;
	Super.PostLogin(PC);
	foreach WorldInfo.AllPawns(class'Pawn', P)
	{
		PC.Possess(P, false);
	}
	PC.ResetCameraMode();
}

DefaultProperties
{
	PlayerControllerClass=class'hanst.MyPC' // name of your PlayerController
	HUDType=class'hanst.MyHUD' // your HUD
	bRestartLevel=true // restart level when player dies
	bDelayedStart=false // do not have a warmup
}