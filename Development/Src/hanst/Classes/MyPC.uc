class MyPC extends GamePlayerController
placeable;

/** Holds the dimensions of the device's screen */
var vector2D ViewportSize;

/** If TRUE, a new touch was detected (must be the only touch active) */
var bool bPendingTouch;

/** Holds the handle of the most recent touch */
var int PendingTouchHandle;

/** Holds the Actor that was selected */
var Actor SelectedActor;

/** Maximum distance an Actor can be to be picked */
var float PickDistance;

/** Maximum amount the mouse can move between touch and untouch to be considered a 'click' */
var float ClickTolerance;

/** Cache a reference to the MobilePlayerInput */
var MobilePlayerInput MPI;
var MobileInputZone StickMoveZone;
var MobileInputZone LookZone;
var MobileInputZone OtherLookZone;

var Vector2D previous;
var int currentHandles;
var Vector2D diff;

var int stepsSinceTouch;

var name currentCamera;

/** Keeps of the initial point of contact for a given touch, used to tell
where a touch started at*/
const NumTouchDataEntries		= 5;
var vector2d InitialTouchLocations[NumTouchDataEntries];


delegate bool ProcessLookZone(MobileInputZone Zone, float DeltaTime, int Handle, EZoneTouchEvent EventType, Vector2D TouchLocation)
{
	if(currentHandles >= 3)
		return true;
	if(currentHandles >= 2)
		return true;
		
	return false;
}

delegate bool ProcessMoveZone(MobileInputZone Zone, float DeltaTime, int Handle, EZoneTouchEvent EventType, Vector2D TouchLocation)
{
	if(currentHandles >= 4)
	return true;
	return false;
}

delegate bool ProcessOtherLookZone(MobileInputZone zone, float deltaTime, int handle, EZoneTouchEvent eventType, Vector2D touchLocation)
{
	if(currentHandles >= 4)
	return true;
	return false;
}

function HandleInputTouch(int handle, EZoneTouchEvent type, Vector2D touchLocation, float deviceTimestamp, int touchpadindex)
{
	local array<MobileInputZone> zones;
	local TouchData data;
	local vector2d dif;
	local float vectorLength;
	local int i;
	local bool changeCamera;
	//local vector2d up;
	//up = vect2d(0, 1);
	//WorldInfo.Game.Broadcast(self, "Current mobile group " $ MPI.CurrentMobileGroup);
	//zones = MPI.CurrentInputGroup.AssociatedZones;
	
	//WorldInfo.Game.Broadcast(self, "before There are currently " $ zones.Length $ " zones.");
	//zones.Remove(0, 1);	
	//WorldInfo.Game.Broadcast(self, "There are currently " $ zones.Length $ " zones.");
	if(handle == 0)
	{
		//diff = (touchLocation - previous);
		//WorldInfo.Game.Broadcast(self, "Direction x:" $ diff.x $ " y: " $ diff.y);//"Handle " $ handle $ " with type " $ type $ "x:" $ touchLocation.x $ " y: " $ touchLocation.y $ " with touch pad index " $ touchpadindex);
	}
	
	if(type == ZoneEvent_Touch)
	{
		currentHandles++;
		InitialTouchLocations[handle] = touchLocation;
		if(currentHandles <= 2)
		{
			MPI.ActivateInputGroup("Look");
		}
		else
		{
			MPI.ActivateInputGroup("OtherLook");
		}
		
		if(currentHandles == 4)
		{
			//SetCameraMode('Top');
		}
		else
		{
			//SetCameraMode('ThirdPerson');
		}
		
		if(currentHandles == 1)
		{
			WorldInfo.Game.Broadcast(self, "Activating look zone");
			LookZone.SizeX = 10000;
			LookZone.SizeY = 10000;
			//StickMoveZone.DeactivateZone();
			//MPI.ActivateInputGroup("Look");
			//LookZone.ActivateZone();
			
			// if(zones.Find(LookZone) == -1)
			
				// zones.AddItem(LookZone);
			// zones.RemoveItem(StickMoveZone);
		}
		else if(currentHandles == 2)
		{
			WorldInfo.Game.Broadcast(self, "Activating move zone");
			StickMoveZone.CurrentLocation  = touchLocation;
			StickMoveZone.InitialCenter  = touchLocation;
			StickMoveZone.CurrentCenter = touchLocation;
			StickMoveZone.bCenterOnEvent = true;
			StickMoveZone.SizeX = 300;
			StickMoveZone.SizeY = 300;
			LookZone.SizeX = 0;
			LookZone.SizeY = 0;
			LookZone.bCenterOnEvent = true;
			//LookZone.VertMultiplier = 0;
			//LookZone.HorizMultiplier = 0;
			StickMoveZone.X = touchLocation.x - StickMoveZone.SizeX / 2;
			StickMoveZone.Y = touchLocation.y - StickMoveZone.SizeY / 2;
			//LookZone.DeactivateZone();
			//MPI.ActivateInputGroup("Move");
			//if(zones.Find(StickMoveZone) == -1)
			//	zones.AddItem(StickMoveZone);
			//zones.RemoveItem(LookZone);
			//LookZone.DeactivateZone();
			//StickMoveZone.ActivateZone();
		}	
	}
	if(type == ZoneEvent_Update)
	{
		if(currentHandles == 2)
		{
			//zones.RemoveItem(LookZone);
			//LookZone.InitialCenter = touchLocation;
		}
		//	PlayerInput.aTurn += (touchLocation.x - previous.x) * 0.1;
		//	PlayerInput.aLookUp += (touchLocation.y - previous.y) * 0.1;
		
	}
	if(type == ZoneEvent_UnTouch)
	{
		if(currentHandles == 4)
		{
			changeCamera = true;
			//Loop over each handle, and check that the y value is > in each case
			for(i = 0; i < 4; i++)
			{
				dif = InitialTouchLocations[i] - MPI.Touches[i].Location;
				changeCamera = changeCamera && (dif.y > 0) && MPI.Touches[i].TotalMoveDistance > 50;
			}
			if(changeCamera)
			{
				if(currentCamera == 'ThirdPerson')
				{
					currentCamera = 'Top';
					`log("Changing to top");
				}
				else
				{
					currentCamera = 'ThirdPerson';
					`log("Changing to third person");
				}
				SetCameraMode(currentCamera);
			}
		}
		currentHandles--;
		if(currentHandles == 0)
		{
			//StickMoveZone.SizeX = 0;
			//StickMoveZone.SizeY = 0;
			//LookZone.SizeX = 0;
			//LookZone.SizeY = 0;
			//MPI.ActivateInputGroup("Look");
		}
		//Broadcast the distance the touch has gone. Ultimately. I want to just figure out what I can get from it.
		
		data = MPI.Touches[handle];
		dif = InitialTouchLocations[i] - touchLocation;
		vectorLength = Sqrt(dif.x * dif.x + dif.y * dif.y);
		WorldInfo.Game.Broadcast(self, "Distance of handle: " $ handle $ " is " $ data.TotalMoveDistance $ " and has straight line dif " $ vectorLength $ " with type " $ type $ "x:" $ touchLocation.x $ " y: " $ touchLocation.y $ " with touch pad index " $ touchpadindex $ " and has " $ data.Events.Length $ " events.", 'Say');
	}
	if(handle == 0)
	previous = touchLocation;
}

event PlayerTick(float delta)
{
	// diff *= delta;
	// if(currentHandles == 2 && !IsStickMoveActive())
	// {
		// PlayerInput.aForward -= diff.y;
		// PlayerInput.aStrafe += diff.x;
	// }
	// WorldInfo.Game.Broadcast(self, "Tick with diff vector x: " $ diff.x $ " and with y: " $ diff.y);
	Super.PlayerTick(delta);
}

function bool IsStickMoveActive()
{
	return StickMoveZone.State != ZoneState_Inactive;
}

function ActivateLook(bool active)
{
	if(active)
	{
		MPI.ActivateInputGroup("Look");
	}
}

event InitInputSystem()
{
	Super.InitInputSystem();
	
	MPI = MobilePlayerInput(PlayerInput);
	MPI.OnInputTouch = HandleInputTouch;

	StickMoveZone = MPI.FindZone("UberStickMoveZone");
	StickMoveZone.OnProcessInputDelegate = ProcessMoveZone;
	LookZone = MPI.FindZone("UberLookZone");
	LookZone.OnProcessInputDelegate = ProcessLookZone;
	MPI.ActivateInputGroup("Directions");
	
	OtherLookZone = MPI.FindZone("UberLookZone1");
	OtherLookZone.OnProcessInputDelegate = ProcessOtherLookZone;

	WorldInfo.Game.Broadcast(self, "init finished");
}

DefaultProperties
{
	CameraClass = class'ThirdPersonCamera'
	bReplicateAllPawns = true
	InputClass = class'GameFramework.MobilePlayerInput'
	currentCamera = 'ThirdPerson';
}