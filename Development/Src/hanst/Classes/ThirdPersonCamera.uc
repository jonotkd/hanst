/**
 *	3rdPersonCamera
 *
 *	Creation date: 09/10/2011 16:09
 *	Copyright 2011, jonynz
 */
class ThirdPersonCamera extends GamePlayerCamera;

protected function GameCameraBase FindBestCameraType(Actor CameraTarget)
{
	local GameCameraBase BestCam;
	if(CameraStyle == 'ThirdPerson')
	{
		BestCam = ThirdPersonCam;	
	}
	else if(CameraStyle == 'Top')
	{
		BestCam = FixedCam;
	}
	
	return BestCam;
	return super.FindBestCameraType(CameraTarget);
	//return ThirdPersonCam;
}

defaultproperties
{
	ThirdPersonCameraClass = class'hanst.SandBox3rdPersonCamera'
	FixedCameraClass = class'GameFixedCamera'
	CameraStyle = 'ThirdPerson'
}
