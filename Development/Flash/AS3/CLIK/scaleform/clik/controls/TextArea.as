﻿/**
 * TextArea is an editable text field component that supports multi-line text and an optional ScrollBar. It is derived from the CLIK TextInput component and thus includes all of the functionality and properties of TextInput. TextArea also shares the same states as its parent component.
 
    <b>Inspectable Properties</b>
    The inspectable properties of the TextArea component are similar to TextInput with a couple of additions and the omission of the password property. The additions are related to the CLIK ScrollBar component:
    <ul>
        <li><i>text</i>: Sets the text of the textField.</li>
        <li><i>visible</i>: Hides the component if set to false.</li>
        <li><i>disabled</i>: Disables the component if set to true.</li>
        <li><i>editable</i>: Makes the TextInput non-editable if set to false.</li>
        <li><i>maxChars</i>: A number greater than zero limits the number of characters that can be entered in the textField.</li>
        <li><i>scrollBar</i>: Instance name of the CLIK ScrollBar component to use, or a linkage ID to the ScrollBar symbol – an instance will be created by the TextArea in this case.</li>
        <li><i>scrollPolicy</i>: When set to “auto” the scrollBar will only show if there is enough text to scroll. The ScrollBar will always display if set to “on”, and never display if set to “off”. This property only affects the component if a ScrollBar is assigned to it (see the scrollBar property).</li>
        <li><i>defaultText</i>: Text to display when the textField is empty. This text is formatted by the defaultTextFormat object, which is by default set to light gray and italics.</li>
        <li><i>actAsButton</i>: If true, then the TextArea will behave similar to a Button when not focused and support rollOver and rollOut states. Once focused via mouse press or tab, the TextArea reverts to its normal mode until focus is lost.</li>
        <li><i>enableInitCallback</i>: If set to true, _global.CLIK_loadCallback() will be fired when a component is loaded and _global.CLIK_unloadCallback will be called when the component is unloaded. These methods receive the instance name, target path, and a reference the component as parameters.  _global.CLIK_loadCallback and _global.CLIK_unloadCallback should be overriden from the game engine using GFx FunctionObjects.</li>
    </ul>

    <b>States</b>
    Like its parent, TextInput, the TextArea component supports three states based on its focused and disabled properties.
    <ul>
        <li>default or enabled state.</li>
        <li>focused state, typically a represented by a highlighted border around the textField.</li>
        <li>disabled state.</li>
    </ul>
*/

/**************************************************************************

Filename    :   TextArea.as

Copyright   :   Copyright 2011 Autodesk, Inc. All Rights reserved.

Use of this software is subject to the terms of the Autodesk license
agreement provided at the time of installation or download, or which
otherwise accompanies this software in either electronic or hard copy form.

**************************************************************************/
 
package scaleform.clik.controls {
    
    import flash.display.DisplayObject;
    import flash.display.Sprite;
    import scaleform.clik.events.ComponentEvent;
    import scaleform.clik.controls.TextInput;
    
    import flash.events.KeyboardEvent;
    import flash.events.Event;
    import flash.text.TextField;
    import flash.utils.getDefinitionByName;
    import flash.events.MouseEvent;
    
    import scaleform.clik.ui.InputDetails;
    import scaleform.clik.constants.InputValue;
    import scaleform.clik.constants.NavigationCode;
    
    import scaleform.clik.events.InputEvent;
    import scaleform.clik.interfaces.IScrollBar;
    import scaleform.clik.constants.InvalidationType;
    
    public class TextArea extends TextInput {
        
    // Constants:
    
    // Public Properties:
    
    // Protected Properties:
        protected var _scrollPolicy:String = "auto";
        
        protected var _position:int = 1;
        protected var maxscroll:Number = 1;
        protected var resetScrollPosition:Boolean = false;
        
        protected var autoScrollBar:Boolean = false;
        
    // UI Elements:
        protected var _scrollBar:IScrollBar;
        protected var container:Sprite;

    // Initialization:
        public function TextArea() {
            super();
        }
        
        override protected function initialize():void {
            super.initialize();
        }
        
    // Public getter / setters:
        [Inspectable(defaultValue="true")]
        override public function get enabled():Boolean { return super.enabled; }
        override public function set enabled(value:Boolean):void {
            super.enabled = value;
            updateScrollBar();
        }
        
        public function get position():int { return _position; }
        public function set position(value:int):void {
            _position = value;
            textField.scrollV = _position;
        }
        
        [Inspectable(name="scrollPolicy", defaultValue="auto", type="list", enumeration="auto,on,off")]
        public function get scrollPolicy():String { return _scrollPolicy; }
        public function set scrollPolicy(value:String):void {
            _scrollPolicy = value;
            updateScrollBar();
        }
        
        [Inspectable(type="String", defaultValue="CLIKScrollBar")]
        public function get scrollBar():Object { return _scrollBar; }
        public function set scrollBar(value:Object):void {
            if (_scrollBar != null) {
                _scrollBar.removeEventListener(Event.SCROLL, handleScroll);
                _scrollBar.removeEventListener(Event.CHANGE, handleScroll);
                _scrollBar.focusTarget = null;
                
                // Attempt to remove any auto-generated scrollbar.
                // nfm: Potentially add an if (autoScrollBar) here and to ScrollingList.
                var sb_DisplayObject:DisplayObject = _scrollBar as DisplayObject;
                if (container.contains(sb_DisplayObject)) { container.removeChild(sb_DisplayObject); }
            }
            
            autoScrollBar = false; // Reset
            trace(">>>>>>>>>>>>> " + this + " :: set scrollBar(" + value + ")!");
            var sb:IScrollBar;
            if (value is String && value != "") {
                if (parent != null) {
                    sb = parent.getChildByName(value.toString()) as IScrollBar;
                }
                if (sb == null) {
                    trace(">>>> ScrollBar String was provided, but no ScrollBar was found. Looking up class by " + value.toString());
                    var classRef:Class = getDefinitionByName(value.toString()) as Class;
                    trace(">>>> classRef: " + classRef);
                    if (classRef != null) { sb = new classRef() as IScrollBar; }
                    if (sb != null) {
                        autoScrollBar = true;
                        sb.addEventListener(MouseEvent.MOUSE_WHEEL, blockMouseWheel, false, 0, true); // Prevent duplicate scroll events
                        //if (sb.scale9Grid == null) { sb.scale9Grid = new Rectangle(0,0,1,1); } // Prevent scaling
                        trace(">>>>> Adding a new ScrollBar to the Stage: " + sb);
                        trace(">>>>> container: " + container);
                        container.addChild(sb as DisplayObject);
                        trace(">>>>> ScrollBar has been added to the Stage: " + sb);
                    }
                }
            } else if (value is Class) {
                sb = new (value as Class)() as IScrollBar;
                sb.addEventListener(MouseEvent.MOUSE_WHEEL, blockMouseWheel, false, 0, true);
                if (sb != null) {
                    autoScrollBar = true;
                    container.addChild(sb as DisplayObject);
                }
            } else {
                sb = value as IScrollBar;
            }
            
            trace(">>>>> Completed setting up scrollBar: "  + _scrollBar);
            _scrollBar = sb;
            
            // nfm: Necessary?
            invalidateSize(); // Redraw to reset scrollbar bounds, even if there is no scrollBar.
            
            if (_scrollBar == null) { return; }

            // Now that we have a scrollBar, lets set it up.
            _scrollBar.addEventListener(Event.SCROLL, handleScroll);
            _scrollBar.addEventListener(Event.CHANGE, handleScroll);
            _scrollBar.focusTarget = this;

            var sb_Indicator:ScrollIndicator = (_scrollBar as ScrollIndicator);
            sb_Indicator.scrollTarget = textField;
            
            maxscroll = textField.maxScrollV;
            updateScrollBar();
            
            _scrollBar.tabEnabled = false;
            
            // nfm: From the AS2 TextArea:
            // changeLock = true;
            // onChanged();
            // changeLock = false;
        }
        
    // Public Methods:
        override public function toString():String {
            return "[CLIK TextArea " + name + "]";
        }
        
        override public function handleInput(event:InputEvent):void {
            super.handleInput(event);
            if (event.handled) { return; }
            else if (_editable) { return; }
            else {
                var navEquivalent:String = event.details.navEquivalent;
                switch(navEquivalent) {
                    case NavigationCode.UP:
                        if (position == 1) { return; }
                        position = Math.max(1, position - 1);
                        event.handled = true;
                        break;
                        
                    case NavigationCode.DOWN:
                        if (position == maxscroll) { return; }
                        position = Math.min(maxscroll, position + 1);
                        event.handled = true;
                        break;

                    case NavigationCode.END:
                        position = maxscroll;
                        event.handled = true;
                        break;
                    
                    case NavigationCode.HOME:
                        position = 1;
                        event.handled = true;
                        break;
                    
                    case NavigationCode.PAGE_UP:
                        var pageSize_up:Number = textField.bottomScrollV - textField.scrollV;
                        position = Math.max(1, position - pageSize_up);
                        event.handled = true;
                        break;
                    
                    case NavigationCode.PAGE_DOWN:
                        var pageSize_down:Number = textField.bottomScrollV - textField.scrollV;
                        position = Math.min(maxscroll, position + pageSize_down);
                        event.handled = true;
                        break;
                }
            }
        }
            
    // Private Methods:
        override protected function configUI():void {
            super.configUI();

            // Mouse.addListener(this);
            
            if (container == null) {
                container = new Sprite();
                addChild(container);
                
                //LM: We can't apply a grid this way like we could in AS2. Revisit if we have scaling issues.
                //container.scale9Grid = new Rectangle(0,0,0,0);
            }
            
            // nfm: This causes a crash in 4.0.42, probably because the class is invalid (symbol isn't in the library). Add more checks to set scrollBar()?
            // nfm: This is required. If the user actually wants to use the defaultValue ("CLIKScrollBar") this is what properly sets it.
            if (_scrollBar == null) { scrollBar = "CLIKScrollBar"; }
            
            if (textField) {
                textField.addEventListener(Event.SCROLL, onScroller, false, 0, true);
            }
        }
        
        override protected function draw():void {
            trace("draw()!");
            super.draw();

            // nfm: Mimic super.draw()'s setup.
            if (isInvalid(InvalidationType.STATE)) {
                //
            }
            else if (isInvalid(InvalidationType.DATA)) {
                updateScrollBar();
            }
            
            if (isInvalid(InvalidationType.SIZE)) {
                // nfm: This logic needs to be ported to AS3 to reflect scaleX/Y 0-1 rather than AS2 0-100.
                // container.scaleX = 10000 / scaleX;
                // container.scaleY = 10000 / scaleY;
                
                removeChild(container);
                // removeChild(textField);
                // setActualSize(_width, _height);
                
                textField.scaleX = 1 / scaleX;
                textField.scaleY = 1 / scaleY;
                
                container.scaleX = 1 / scaleX;
                container.scaleY = 1 / scaleY;
                
                // addChild(textField);
                addChild(container);

                if (autoScrollBar) {
                    _scrollBar.x = width - _scrollBar.width; // Using the _width throughout to avoid initialization issues.
                    _scrollBar.height = height - 1;
                }
            }
        }
        
        protected function updateScrollBar():void {
            trace("updateScrollBar()");
            maxscroll = textField.maxScrollV;
            if (_scrollBar == null) { return; }
            if (_scrollBar is ScrollIndicator) {
                var scrollIndicator:ScrollIndicator = _scrollBar as ScrollIndicator;
                scrollIndicator.setScrollProperties( (textField.bottomScrollV - textField.scrollV), 1, textField.maxScrollV);
            } else {
                // min/max
            }
            
            // _scrollBar.position = _scrollPosition; // nfm: from ScrollingList.
            _scrollBar.position = _position;
            _scrollBar.validateNow();
        }
        
        override protected function updateText():void {
            trace("updateText()!");
            super.updateText();
            updateScrollBar();
        }
        
        override protected function updateTextField():void {
            trace("updateTextField()");
            resetScrollPosition = true;
            super.updateTextField();
            
            // nfm: Necessary?
            /*
            if (textField != null) {
                if (_scrollBar != null) { 
                    var sb:ScrollIndicator = (_scrollBar as ScrollIndicator);
                    sb.scrollTarget = textField; 
                }
            }
            */
        }
        
        // handle SCROLL and CHANGE events fired by the scrollbar.
        protected function handleScroll(event:Event):void {
            trace("\nhandleScroll() -- SCROLL or CHANGE event fired by the scrollBar!");
            position = _scrollBar.position;
        }
        
        protected function blockMouseWheel(event:MouseEvent):void {
            trace("blockMouseWheel");
            event.stopPropagation();
        }
        
        override protected function handleTextChange(event:KeyboardEvent):void {
            trace("\n"+this+"::handleTextChange()");
            if (maxscroll != textField.maxScrollV) {
                 updateScrollBar();
            }
            super.handleTextChange(event);
        }
        
        // handle SCROLL events fired by the textField (mouseWheel?)
        protected function onScroller(event:Event):void {
            trace("onScroller!");
            if (resetScrollPosition) { textField.scrollV = _position; }
            else { _position = textField.scrollV; }
            resetScrollPosition = false;        
            
            // nfm: I'm not sure this is necessary. Event should just bubble?
            // var newEvent:ComponentEvent = new ComponentEvent("scroll", true);
            // dispatchEventAndSound(newEvent);
        }
    
        /*
        protected function scrollWheel(delta:Number):void {
            trace("scrollWheel( " + delta + ")");
            position = Math.max(1, Math.min(maxscroll, _position - delta));
        }
        */
    }
}