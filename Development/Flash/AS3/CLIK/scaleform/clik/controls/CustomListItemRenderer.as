﻿package scaleform.clik.controls {
	
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	
	import scaleform.clik.core.UIComponent;
	import scaleform.clik.data.ListData;
	import scaleform.clik.events.InputEvent;
	import scaleform.clik.constants.InputValue;
	import scaleform.clik.constants.NavigationCode;
	import scaleform.clik.utils.Constraints;
	import flash.display.DisplayObject;
	import flash.geom.Point;
	import flash.ui.Keyboard;
	import flash.text.TextField;
	
	public class CustomListItemRenderer extends ListItemRenderer {
		
	// Constants:
		
	// Public Properties:
		
	// Protected Properties:
		
	// UI Elements:
	
		public var checkBox:CheckBox;
	
	// Initialization:
	
		public function CustomListItemRenderer() {
			super();
		}
		
	// Public getter / setters:
	
		override public function set enabled(value:Boolean):void {
			super.enabled = value;
			toggleCheckBoxEnabled(enabled);
		}
		
	// Public Methods:
	
		override public function setListData(listData:ListData):void {
			super.setListData(listData);
			toggleCheckBoxEnabled(enabled);
		}
		
		override public function setData(data:Object):void {
			super.setData(data);
			if (data != null) {
				toggleCheckBoxSelected(data.isToggled);
			}
		}
		
		override public function toString():String {
			return "[CLIK CustomListItemRenderer " + index + "]";
		}
		
		public function toggleCheckBoxEnabled(value:Boolean):void {
			if (checkBox == null) { return; }
			checkBox.enabled = value;
			if (invalid) { checkBox.validateNow(); }
			if (!checkBox.enabled) { toggleCheckBoxSelected(false); }
		}
		
		public function toggleCheckBoxSelected(value:Boolean):void {
			if (checkBox == null) { return; }
			checkBox.selected = value;
		}
		
		public function toggleData(value:Boolean):Boolean {
			if (data == null) { data = {}; }
			data.isToggled = value;
			return data.isToggled;
		}
		
	// Protected Methods:
		
		override protected function initialize():void {
			data = {isToggled:false};
			super.initialize();
		}
	
		override public function handleInput(event:InputEvent):void {
			super.handleInput(event);
			if (_selected && event.details.value == InputValue.KEY_UP && event.details.navEquivalent == NavigationCode.ENTER) {
				toggleCheckBoxSelected(toggleData(!data.isToggled));
			}
		}
	
		override protected function configUI():void {
			super.configUI();
			mouseChildren = true;
			if (checkBox != null) {
				constraints.addElement("checkBox", checkBox, Constraints.RIGHT);
				checkBox.addEventListener(Event.SELECT, handleCheckBoxSelect, false, 0, true);
				checkBox.addEventListener(MouseEvent.CLICK, handleCheckBoxClick, false, 0, true);
			}
			
			addEventListener(MouseEvent.MOUSE_UP, handleMouseUp, false, 0, true);
		}
		
		protected function handleMouseUp(event:MouseEvent):void {
			if (_selected) {
				toggleData(!data.isToggled);
			}
		}
		
	// Mouse-only input:
	
		override protected function handleMouseRelease(event:MouseEvent):void {
			super.handleMouseRelease(event);
			if (_selected) {
				toggleCheckBoxSelected(toggleData(!checkBox.selected));
			}
		}
		
		protected function handleCheckBoxClick(event:MouseEvent):void {
			event.stopPropagation(); // Prevents the CustomListItemRenderer from being selected.
		}
		
		protected function handleCheckBoxSelect(event:Event):void {
			if (enabled) {
				toggleData(checkBox.selected);
			}
		}
	}
	
}