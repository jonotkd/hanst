/**
 *  Event structure and defintions for the CLIK ScrollingList: 
 *  Valid types:
 *      ITEM_CLICK - "indexChange"
 *      ITEM_PRESS - "itemPress"
 *      ITEM_ROLL_OVER - "itemRollOver"
 *      ITEM_ROLL_OUT - "itemRollOut"
 *      ITEM_DOUBLE_CLICK - "itemDoubleClick"
 *      INDEX_CHANGE - "indexChange"
 */

/**************************************************************************

Filename    :   ListEvent.as

Copyright   :   Copyright 2011 Autodesk, Inc. All Rights reserved.

Use of this software is subject to the terms of the Autodesk license
agreement provided at the time of installation or download, or which
otherwise accompanies this software in either electronic or hard copy form.

**************************************************************************/
 
package scaleform.clik.events {
    
    import flash.events.Event;
    
    import scaleform.clik.controls.Button;
    import scaleform.clik.interfaces.IListItemRenderer;
    
    public class ListEvent extends Event {
        
    // Constants:
        public static const ITEM_CLICK:String = "itemClick";
        public static const ITEM_PRESS:String = "itemPress";
        public static const ITEM_ROLL_OVER:String = "itemRollOver";
        public static const ITEM_ROLL_OUT:String = "itemRollOut";
        public static const ITEM_DOUBLE_CLICK:String = "itemDoubleClick";
        public static const INDEX_CHANGE:String = "indexChange"; //LM: Evaluate.
        
    // Public Properties:
        public var index:int = -1;
        public var columnIndex:int = -1;
        public var rowIndex:int = -1;
        public var itemRenderer:IListItemRenderer;
        public var isKeyboard:Boolean = false;
        
    // Protected Properties:
        
    // Initialization:
        public function ListEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=true, 
                                  index:int=-1, columnIndex:int=-1, rowIndex:int=-1, 
                                  itemRenderer:IListItemRenderer=null, isKeyboard:Boolean = false) {
            super(type, bubbles, cancelable);
            
            this.index = index;
            this.rowIndex = rowIndex;
            this.columnIndex = columnIndex;
            this.itemRenderer = itemRenderer;
            this.isKeyboard = isKeyboard;
        }
        
    // Public getter / setters:
        
    // Public Methods:
        override public function clone():Event {
            return new ListEvent(type, bubbles, cancelable, index, columnIndex, rowIndex, itemRenderer, isKeyboard);
        }
        
        override public function toString():String {
            return formatToString("ListEvent", "type", "bubbles", "cancelable", "index", "columnIndex", "rowIndex", "isKeyboard");
        }
        
    // Protected Methods:
        
    }
    
}